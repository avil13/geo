<?php

$string = file_get_contents("addrs_houses.json");
$json = json_decode($string, true);

$geo = array();
$i = 0;

foreach($json as $address) {
		
	$geo[$i]['ADDR_FULL'] = $address['ADDR_FULL'];
	$geo[$i]['ADDR_ID'] = $address['ADDR_ID'];
	
	$geocode = file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&results=1&geocode='. urlencode('Москва, '.$address['ADDR_FULL']));
	$data = json_decode($geocode, true);
	
	if (isset($data['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'])) {
		list(,$latitude) = explode(' ', $data['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
		$geo[$i]['ADDR_LAT'] = (float)$latitude;
		list($longitude,) = explode(' ', $data['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
		$geo[$i]['ADDR_LONG'] = (float)$longitude;
		$geo[$i]['ADDR_COORDS'] = $data['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
	}
	
	$i++;
}

echo '<pre>';
print_r($geo);
echo '</pre>';

$fp = fopen('results.json', 'w');
fwrite($fp, json_encode($geo));
fclose($fp);

?>