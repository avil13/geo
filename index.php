<?php

// use Illuminate\Database\Capsule\Manager as Capsule;


require_once('vendor/autoload.php');
require_once('config/database.php');



function arr_val($data, $str){
    $keys = explode('.', $str);

    foreach ($keys as $v) {
        if(!empty($data) && !empty($data[$v])){
            $data = $data[$v];
        }else{
            return false;
        }
    }

    return $data;
}



// функция для запросов
function req($addr, $req, $retry = false) {
    $url = 'http://geocode-maps.yandex.ru/1.x/?format=json&results=1&geocode=' . urlencode('Москва, '. $addr->addr_full);

    $res = $req->get($url);

    if(!$res){
        return false;
    }

    $data = json_decode($res, true);

    // получаем координаты
    // $pos = $data['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
    $pos = arr_val($data, 'response.GeoObjectCollection.featureMember.0.GeoObject.Point.pos');

    if($pos !== false){
        $addr->coord = $pos;
        $addr->status = true;
        return $addr->save();
    }

    if($retry === false){
        sleep(0.2);
        return req($addr, $req, true);
    }

    // меняем прокси
    $proxy = Proxy::where('failed', false)->first();
    $count = Proxy::where('failed', false)->count();

    if (empty($proxy)){
        if($count === 0){
            fwrite(STDOUT, "\nЗакончились Proxy \n");
            exit(1);
        }
        return false;
    }

    $proxy->failed = true;
    $proxy->save();

    $req->setProxy($proxy->ip);
    fwrite(STDOUT, "\n[{$count}] new proxy: {$proxy->ip} . . . ". date("Y-m-d H:i:s") ."\n");

    return req($addr, $req); // повторный запрос
}





$req = new Request;


$cnt = Geo::count();
$complete = Geo::where('status', true)->count();
$left = Geo::where('status', false)->count();
$fail = 0;



//
fwrite(STDOUT, "Count:[". number_format($cnt) ."]\n");
fwrite(STDOUT, "\0337"); // Save position

// получаем адрес
$addrList = Geo::where('coord', '')->where('status', false)->orderBy('id', 'DESC')->get();


foreach ($addrList as  $v) {
    fwrite(STDOUT, "\0338". "id:[{$v->id}]  fail:[{$fail}] complete:[{$complete}] left:[{$left}] . . . ". number_format($complete / ($cnt / 100), 2) . " %");

    $res = req($v, $req);

    if(!$res){
        ++$fail;
    }else{
        --$left;
        ++$complete;
    }
}


//

fwrite(STDOUT, PHP_EOL);
