# Получение координат адресов из Яндекса

```
git clone git@gitlab.com:avil13/geo.git
cd geo
composer install
```
Далее создаете БД, структура ее лежит в папке `_db`, нужно сделать импорт перед началом.

Далее в файле `config/database.php` прописываете настройки соединения с БД

Если все прошло удачно то запускаете скрипт
```
php index.php
```

Он начнет парсить координаты, если запрос не будет проходить, то он попытается его выполнить через прокси.

Соответствующие прокси адреса лежат в БД. Их можно пополнять, нужно использовать socks4.



***
- https://packagist.org/packages/illuminate/database
- https://laracasts.com/lessons/how-to-use-eloquent-outside-of-laravel
