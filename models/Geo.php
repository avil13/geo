<?php

use Illuminate\Database\Eloquent\Model;


/**
*  Класс для работы с Таблицей гео данных
*/
class Geo extends Model
{
    protected $table = 'geo';

    public $timestamps = false;

    protected $fillable = ['addr_id', 'addr_full', 'coord', 'status'];
}

