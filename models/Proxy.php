<?php

use Illuminate\Database\Eloquent\Model;


/**
*  Класс для работы с Таблицей гео данных
*/
class Proxy extends Model
{
    protected $table = 'proxy';

    public $timestamps = false;

    protected $fillable = ['ip', 'failed'];
}

